# Contributing

## Reporting Bugs

Please file a [GitLab Issue](https://gitlab.com/jlecomte/images/docker-molecule/issues/new) with a clear description of the issue. Explain the problem and include additional details to help maintainers reproduce the problem:

* __Use a clear and descriptive title__ for the issue to identify the problem.
* __Describe the exact steps which reproduce the problem__ in as many details as possible.
* __Describe the behavior you observed__ and the __behavior you expected__.

## Submitting changes

Please create a [GitLab Merge Request](https://gitlab.com/jlecomte/images/docker-molecule/merge_requests/new) with a clear list of what you've done and why it is needed. Please verify that:

* Continuous Integration is passing,
* Unit tests are passing and new unit tests exist for the new feature,
* Commit messages are properly written (ie: [the seven rules](https://chris.beams.io/posts/git-commit/#seven-rules)),
* You can contribute your changes to this project with respect to the [LICENSE](LICENSE) of this project.

Read more about [merge requests](https://docs.gitlab.com/ce/gitlab-basics/add-merge-request.html).

