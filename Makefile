DOCKER_NAMESPACE ?= julienlecomte
DOCKER_IMAGE     := docker-molecule
DOCKER_TAG       ?= $(shell git rev-parse --abbrev-ref HEAD)

# git says 'master', docker wants 'latest'
ifeq ($(DOCKER_TAG),master)
DOCKER_TAG := latest
endif

DOCKER_LOCAL ?= $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE)

#-------------------------------------------------------------------------------
# Google Container Tools 'container-structure-test'
#
# https://github.com/GoogleContainerTools/container-structure-test
UT_IMAGE ?= gcr.io/gcp-runtimes/container-structure-test
UTFLAGS  ?= --quiet

#-------------------------------------------------------------------------------
all: image

build: image

image:
	docker build $(DOCKERFLAGS) --tag $(DOCKER_LOCAL):$(DOCKER_TAG) .

check: image
	-docker pull $(UT_IMAGE)
	docker run --rm \
	  -v /var/run/docker.sock:/var/run/docker.sock \
	  -v "$(CURDIR)/test.yml:/test.yml:ro" \
	  $(UT_IMAGE) test $(UTFLAGS) --image $(DOCKER_LOCAL):$(DOCKER_TAG) --config /test.yml

pull:
	docker pull $(DOCKERFLAGS) $(DOCKER_LOCAL):$(DOCKER_TAG)

push:
	docker push $(DOCKERFLAGS) $(DOCKER_LOCAL):$(DOCKER_TAG)

name:
	@echo -n "$(DOCKER_LOCAL):$(DOCKER_TAG)"

.DEFAULT_GOAL:= all
.PHONY: all check local name pull push

